// modified on 2020-04-10 by 19215510
// Original author: Derek Molloy & Qt


#include "mainwindow.h"
#include "ui_mainwindow.h"
#include<QDebug>
#include <iostream>
#include <stdlib.h>
#include <string>
#include <string.h>



MainWindow *handle;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->count = 50;
    this->time = 0;
    this->setWindowTitle("EE513 Assignment 2");
    this->ui->customPlot->addGraph();
    this->ui->customPlot->yAxis->setLabel("CPU Temp (degrees C)");
    QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
    timeTicker->setTimeFormat("%h:%m:%s");
    this->ui->customPlot->xAxis->setTicker(timeTicker);
    this->ui->customPlot->yAxis->setRange(-180,180);
    this->ui->customPlot->replot();
    QObject::connect(this, SIGNAL(messageSignal(QString)),
                     this, SLOT(on_MQTTmessage(QString)));
    ::handle = this;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::update(){
    // For more help on real-time plots, see: http://www.qcustomplot.com/index.php/demos/realtimedatademo
    static QTime time(QTime::currentTime());
    double key = time.elapsed()/1000.0; // time elapsed since start of demo, in seconds
    ui->customPlot->graph(0)->addData(key,count);
    ui->customPlot->graph(0)->rescaleKeyAxis(true);
    ui->customPlot->replot();
    QString text = QString("Value added is %1").arg(this->count);
    ui->outputEdit->setText(text);
}

void MainWindow::on_downButton_clicked()
{

    this->update();
}

void MainWindow::on_upButton_clicked()
{
    this->count+=10;
    this->update();
}

void MainWindow::on_connectButton_clicked()
{
    MQTTClient_connectOptions opts = MQTTClient_connectOptions_initializer;
    int rc;
    MQTTClient_create(&client, ADDRESS, CLIENTID, MQTTCLIENT_PERSISTENCE_NONE, NULL);
    opts.keepAliveInterval = 20;
    opts.cleansession = 1;
    opts.username = AUTHMETHOD;
    opts.password = AUTHTOKEN;

    QString text_in = ui->lineEdit->text();             //taking text from line edit textbox
    std::string text_in_str = text_in.toStdString();    //converting from qstring to string
	// setting text_in_chars to user inputs; subject based filtering variable
    this->text_in_chars = text_in_str.c_str();
   // std::cout<<text_in_chars<<std::endl;

    if (MQTTClient_setCallbacks(client, NULL, connlost, msgarrvd, delivered)==0){
        ui->outputText->appendPlainText(QString("Callbacks set correctly"));
    }
    if ((rc = MQTTClient_connect(client, &opts)) != MQTTCLIENT_SUCCESS) {
        ui->outputText->appendPlainText(QString("Failed to connect, return code %1").arg(rc));
    }
    QString text_in_qstr = QString::fromUtf8(text_in_chars);
    ui->outputText->appendPlainText(QString("Subscribing to topic "));
    ui->outputText->appendPlainText(QString(text_in_chars));
    ui->outputText->appendPlainText(QString(" for client "));
    ui->outputText->appendPlainText(QString(CLIENTID));
	// text_in_chars is used here to select topic from line edit user input
    int x = MQTTClient_subscribe(client, text_in_chars, QOS);
    ui->outputText->appendPlainText(QString("Result of subscribe is %1 (0=success)").arg(x));
}

void delivered(void *context, MQTTClient_deliveryToken dt) {
    (void)context;
    // Please don't modify the Window UI from here
    qDebug() << "Message delivery confirmed";
    handle->deliveredtoken = dt;
}


void MainWindow::stringJSONParser(QString str){
	//method to parse JSON data from string sent by publisher
   QJsonDocument doc = QJsonDocument::fromJson(str.toUtf8());
   //setting json document to payload sring from publisher
   QJsonObject obj = doc.object();
   //creating multiple JSON objects
   QJsonObject CPUTemp = obj["CPUTemp"].toObject();
   QJsonObject ADXL345 = obj["ADXL345"].toObject();
   QJsonObject pi_time = obj["pi_time"].toObject();
   //conditional statement to ensure object has certain contents within
   //if (CPUTemp.contains("Celius")){std::cout<<"REALREALREALREALREALREALREALREALREALREALREAL"<<std::endl;}
   //else{std::cout<<"AGH£$)GHJI£GH£)(AGJOAGBH£%)(HOJORGH£%(GHASGH£" << std::endl;}
   this->ADXL345_roll = (float) ADXL345["ADXL345_roll"].toDouble();
   this->ADXL345_pitch = (float) ADXL345["ADXL345_pitch"].toDouble();
   this->rpi_seconds = (float) pi_time["Rpi Seconds"].toDouble();
   this->rpi_minutes = (float) pi_time["Rpi Minutes"].toDouble();
   this->rpi_hours = (float) pi_time["Rpi Hours"].toDouble();
   this->temperature = (float) CPUTemp["Celius"].toDouble();
   std::cout << "CPU Temperature: " << this->temperature << " C - " << "ADXL Roll: " << ADXL345_roll
            << " - ADXL Pitch: " << ADXL345_pitch << " - Rpi Time: "
            << rpi_hours << ":" << rpi_minutes << ":" << rpi_seconds << "." <<std::endl;
}
/* This is a callback function and is essentially another thread. Do not modify the
 * main window UI from here as it will cause problems. Please see the Slot method that
 * is directly below this function. To ensure that this method is thread safe I had to
 * get it to emit a signal which is received by the slot method below */
int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message) {
    (void)context; (void)topicLen;
    qDebug() << "Message arrived (topic is " << topicName << ")";
    qDebug() << "Message payload length is " << message->payloadlen;
    QString payload;
    payload.sprintf("%s", message->payload).truncate(message->payloadlen);
    emit handle->messageSignal(payload);
    MQTTClient_freeMessage(&message);
    MQTTClient_free(topicName);
    return 1;
}



/** This is the slot method. Do all of your message received work here. It is also safe
 * to call other methods on the object from this point in the code */
void MainWindow::on_MQTTmessage(QString payload){
    stringJSONParser(payload);
    ui->outputText->appendPlainText(payload);
    ui->outputText->ensureCursorVisible();
    this->count = this->ADXL345_roll;
    this->update();

    //ADD YOUR CODE HERE
}

void connlost(void *context, char *cause) {
    (void)context; (void)*cause;
    // Please don't modify the Window UI from here
    qDebug() << "Connection Lost" << endl;
}

void MainWindow::on_disconnectButton_clicked()
{
    qDebug() << "Disconnecting from the broker" << endl;
    MQTTClient_disconnect(client, 10000);
    //MQTTClient_destroy(&client);
}

void MainWindow::on_lineEdit_returnPressed()
{

}
