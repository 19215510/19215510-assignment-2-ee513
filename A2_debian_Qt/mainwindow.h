// modified on 2020-04-10 by 19215510
// Original author: Derek Molloy & Qt

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDateTime>
#include "MQTTClient.h"

#define ADDRESS     "tcp://127.0.0.1:1883"
#define CLIENTID    "rpi2"
#define AUTHMETHOD  "debian0"
#define AUTHTOKEN   "1"
//#define TOPIC       "ee513/CPUTemp"
#define PAYLOAD     "Hello World!"
#define QOS         1
#define TIMEOUT     10000L

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void stringJSONParser(QString str);
    double temperature;	//class variable for temperature
    double ADXL345_roll;	//ADXL roll as class variable
    double ADXL345_pitch;
	//raspberry pi time from publisher
    int rpi_seconds;		
    int rpi_minutes;
    int rpi_hours;
	// text_in_chars is of char* type and is used for subject based filtering:
	//text_in_chars selects the topic of subscription based on user input
    const char* text_in_chars;
private slots:
    void on_downButton_clicked();
    void on_upButton_clicked();
    void on_connectButton_clicked();
    void on_disconnectButton_clicked();
    void on_MQTTmessage(QString message);
    void on_lineEdit_returnPressed();

signals:
    void messageSignal(QString message);

private:
    Ui::MainWindow *ui;
    void update();
    int count, time;
    MQTTClient client;
    volatile MQTTClient_deliveryToken deliveredtoken;

    friend void delivered(void *context, MQTTClient_deliveryToken dt);
    friend int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message);
    friend void connlost(void *context, char *cause);
};

void delivered(void *context, MQTTClient_deliveryToken dt);
int  msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message);
void connlost(void *context, char *cause);

#endif // MAINWINDOW_H

