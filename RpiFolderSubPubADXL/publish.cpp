// Based on the Paho C code example from www.eclipse.org/paho/
//compile using: 
// g++ publish.cpp I2CDevice.cpp ADXL345.cpp -o this1 -lpaho-mqtt3c
#include <iostream>
#include <sstream>
#include <fstream>
#include <string.h>
#include "MQTTClient.h"
#include <json-c/json.h>
#include "ADXL345.h"
#include <unistd.h>
#include <pthread.h>
#include <time.h>
#define  CPU_TEMP "/sys/class/thermal/thermal_zone0/temp"

using namespace std;
using namespace exploringRPi;	// Namescpace from exploringRpi
// this nemspace is used for scope resolution of ADXL345, as it is within the namespace

//Please replace the following address with the address of your server
#define ADDRESS    "tcp://192.168.1.20:1883"
#define CLIENTID   "rpi1"
#define AUTHMETHOD "debian0"
#define AUTHTOKEN  "1"
#define TOPIC      "ee513/19215510"
#define QOS        2
#define TIMEOUT    10000L
#define DCLASTWILL "Last will message - non-graceful disconnection"

float getCPUTemperature() {        // get the CPU temperature
   int cpuTemp;                    // intiaise int
   fstream fs;
   fs.open(CPU_TEMP, fstream::in); // read from the file
   fs >> cpuTemp;
   fs.close();
   return (((float)cpuTemp)/1000);
}

int getTime(int index){
   time_t seconds;      //number of seconds elapsed since some time ago
   struct tm * parsedtime;
   time ( &seconds );
   parsedtime = localtime ( &seconds ); //parsing time to human readable format
   switch(index){ //to return element of time depending on value passed to function
      case 0:
         return parsedtime->tm_sec;
      case 1:
         return parsedtime->tm_min;
      case 2:
         return parsedtime->tm_hour;
	 }
	 
return 0;
}


int main(int argc, char* argv[]) {
   ADXL345 sensor(1,0x53);	//intialising sensor object
   sensor.setResolution(ADXL345::NORMAL);	//sensor variable initialisation
   sensor.setRange(ADXL345::PLUSMINUS_4_G);

   char str_payload[300];          // Set your max message size here
   MQTTClient client;
   MQTTClient_connectOptions opts = MQTTClient_connectOptions_initializer;
   MQTTClient_message pubmsg = MQTTClient_message_initializer;
   MQTTClient_deliveryToken token;
   MQTTClient_willOptions last_will_options = MQTTClient_willOptions_initializer;
   // setting last will options
   MQTTClient_create(&client, ADDRESS, CLIENTID, MQTTCLIENT_PERSISTENCE_DEFAULT, NULL);
   opts.keepAliveInterval = 20;
   opts.cleansession = 0;        //persistent session if clean flag is 0
   //persistent session -> massages will be cached if subscriber disconnects
   //messeges will be sent once sub reconnects (QoS 1 & 2)
   opts.will = &last_will_options; //assignment of last will options
   opts.username = AUTHMETHOD;
   opts.password = AUTHTOKEN;
   
   last_will_options.topicName = TOPIC;      //last will settings (topic)
   last_will_options.message = DCLASTWILL;   //last will message (defined above)
   last_will_options.qos = QOS;              //last will QOS level (defined above)
   
   int rc;
   if ((rc = MQTTClient_connect(client, &opts)) != MQTTCLIENT_SUCCESS) {
      cout << "Failed to connect, return code " << rc << endl;
      return -1;
   }
   while(1){ //loop to repeat publishing and other related functions
   sensor.readSensorState(); // update readings from sensor
   // string formatted in JSON:
   sprintf(str_payload, 
   "{\
      \"ADXL345\": {\
         \"ADXL345_roll\":%9.6f,\
         \"ADXL345_pitch\":%9.6f\
         },\
      \"pi_time\": {\
         \"Rpi Seconds\":%2d,\
         \"Rpi Minutes\":%2d,\
         \"Rpi Hours\":%2d\
      },\
      \"CPUTemp\": {\
         \"Celius\":%9.6f\
      }\
}"
   , sensor.getRoll(), sensor.getPitch() , getTime(0), getTime(1)
   , getTime(2),getCPUTemperature()); 
   pubmsg.payload = str_payload; // assigning JSON to payload
   pubmsg.payloadlen = strlen(str_payload);
   pubmsg.qos = QOS; // setting Qos
   pubmsg.retained = 0;
   //publishing message
   MQTTClient_publishMessage(client, TOPIC, &pubmsg, &token);
   cout << "Waiting for up to " << (int)(TIMEOUT/1000) <<
        " seconds for publication of " << str_payload <<
        " \non topic " << TOPIC << " for ClientID: " << CLIENTID << endl;
   rc = MQTTClient_waitForCompletion(client, token, TIMEOUT);
   cout << "Message with token " << (int)token << " delivered." << endl;
   //sleep in loop for constant resending (low frequency)
   usleep(1000000); // 1 second sleep
}
   MQTTClient_disconnect(client, 10000);
   MQTTClient_destroy(&client);
   return rc;

}
