//run using: g++ subscribe1.cpp -o subscribe1 -lpaho-mqtt3c -ljson-c
// then: ./subscribe1
// seconds application to action events, check for subscribe0.cpp
// this application actions event, subscribe 0 actuates as well (LED control)

#include "stdio.h"
#include "stdlib.h"
#include <string>
#include <iostream>
#include "MQTTClient.h"
#include <json-c/json.h> //compile with -ljson-c

#define ADDRESS     "tcp://192.168.1.20:1883"
#define CLIENTID    "rpi3"
#define AUTHMETHOD  "debian0"
#define AUTHTOKEN   "1"
#define TOPIC       "ee513/19215510"
#define PAYLOAD     "Hello World!"
#define QOS         2
#define TIMEOUT     10000L

volatile MQTTClient_deliveryToken deliveredtoken;

void delivered(void *context, MQTTClient_deliveryToken dt) {
    printf("Message with token value %d delivery confirmed\n", dt);
    deliveredtoken = dt;
}

int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message) {
    int i;
    char* payloadptr;
    printf("Message arrived\n");
    printf("     topic: %s\n", topicName);
    printf("   message: ");
    //--------------------- JSON PARSING -----------------------
    payloadptr = (char*) message->payload;
    struct json_object *json_parsed0;
    struct json_object *ADXL345_stat;
    char *str = payloadptr;
    json_parsed0 = json_tokener_parse(str);
	printf("JSON after parsing:\n------------\n%s\n------------\n"
    , json_object_to_json_string_ext(json_parsed0
    , JSON_C_TO_STRING_SPACED | JSON_C_TO_STRING_PRETTY));
    json_object_object_get_ex(json_parsed0, "ADXL345", &ADXL345_stat);
    //printf("Stats ADXL345: %s\n", json_object_get_string(ADXL345_stat));
    //----------------------------------------------------------
    struct json_object *json_parsed1;   //second level parsing
    struct json_object *ADXL345_roll;
    const char* lvl_2_ADXL0 = json_object_get_string(ADXL345_stat);
    json_parsed1 = json_tokener_parse(lvl_2_ADXL0);
    
    json_object_object_get_ex(json_parsed1, "ADXL345_roll", &ADXL345_roll);
    const char* lvl_2_ADXL1 = json_object_get_string(ADXL345_roll);
    std::string lvl_2_ADXL2 = std::string(lvl_2_ADXL1);
	double ADXL_roll = (std::atof(lvl_2_ADXL2.c_str()));
    printf("Roll: %f\n",ADXL_roll,ADXL_roll,ADXL_roll);
    
    if(ADXL_roll < 30){
        printf("subsribe1 detected low roll value (below 0)\n");
    }

    //----------------------------------------------------------
    
    putchar('\n');
    MQTTClient_freeMessage(&message);
    MQTTClient_free(topicName);
    return 1;
}

void connlost(void *context, char *cause) {
    printf("\nConnection lost\n");
    printf("     cause: %s\n", cause);
}


int main(int argc, char* argv[]) {
    MQTTClient client;
    MQTTClient_connectOptions opts = MQTTClient_connectOptions_initializer;
    int rc;
    int ch;

    MQTTClient_create(&client, ADDRESS, CLIENTID, MQTTCLIENT_PERSISTENCE_NONE, NULL);
    opts.keepAliveInterval = 20;
    opts.cleansession = 0;  //persistent session if cleansession flag =0
    opts.username = AUTHMETHOD;
    opts.password = AUTHTOKEN;

    MQTTClient_setCallbacks(client, NULL, connlost, msgarrvd, delivered);
    if ((rc = MQTTClient_connect(client, &opts)) != MQTTCLIENT_SUCCESS) {
        printf("Failed to connect, return code %d\n", rc);
        exit(-1);
    }
    printf("Subscribing to topic %s\nfor client %s using QoS%d\n\n"
           "Press Q<Enter> to quit\n\n", TOPIC, CLIENTID, QOS);
    MQTTClient_subscribe(client, TOPIC, QOS);

    do {
        ch = getchar();
    } while(ch!='Q' && ch != 'q');
    MQTTClient_disconnect(client, 10000);
    MQTTClient_destroy(&client);
    return rc;
}
