//compile using: g++ subscribe0.cpp -o subscribe0 -lpaho-mqtt3c -ljson-c
//then: ./subscribe0
// this application actions and actuates event (printf and LED control)

#include "stdio.h"
#include "stdlib.h"
#include <string>
#include <iostream>
#include <fstream>

#include "MQTTClient.h"
#include <json-c/json.h> //compile with -ljson-c

#define ADDRESS     "tcp://192.168.1.20:1883"
#define CLIENTID    "rpi2"
#define AUTHMETHOD  "debian0" //setiing user name for authentication
#define AUTHTOKEN   "1"			//corresponding passwd
#define TOPIC       "ee513/19215510"	//keeping topic as default
#define PAYLOAD     "Hello World!"
#define QOS         2				//setting Qos Level
#define TIMEOUT     10000L
#define LED_PATH    "/sys/class/leds/led0/"	//rpi led control directory

volatile MQTTClient_deliveryToken deliveredtoken;

void ledConditionOn(){		
   std::fstream write; 			
   write.open(LED_PATH "/trigger", std::fstream::out); //
   //change led lighting mode in trigger file
   write << "default-on";	 //setting to ON
   write.close();
}

void ledConditionOff(){
   std::fstream write; 
   write.open(LED_PATH "/trigger", std::fstream::out);
   //change led lighting mode in trigger file
   write << "none"; //no trigger mode - to set brightness to 0 afterwards
   write.close();
}

void delivered(void *context, MQTTClient_deliveryToken dt) {
    printf("Message with token value %d delivery confirmed\n", dt);
    deliveredtoken = dt;
}

int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message) {
    int i;
    char* payloadptr;
    printf("Message arrived\n");
    printf("     topic: %s\n", topicName);
    printf("   message: ");
    //--------------------- JSON PARSING -----------------------
	//------- using jsoc-c -------------------------------------
    payloadptr = (char*) message->payload;
    struct json_object *json_parsed0;
    struct json_object *ADXL345_stat;
    char *str = payloadptr;
    json_parsed0 = json_tokener_parse(str);	//using the library
	// parsing first level of structure
	printf("JSON after parsing:\n------------\n%s\n------------\n"
    , json_object_to_json_string_ext(json_parsed0
    , JSON_C_TO_STRING_SPACED | JSON_C_TO_STRING_PRETTY));
    json_object_object_get_ex(json_parsed0, "ADXL345", &ADXL345_stat);
    //printf("Stats ADXL345: %s\n", json_object_get_string(ADXL345_stat));
    //----------------------------------------------------------
    struct json_object *json_parsed1;   //second level parsing
    struct json_object *ADXL345_roll;
    const char* lvl_2_ADXL0 = json_object_get_string(ADXL345_stat);
    json_parsed1 = json_tokener_parse(lvl_2_ADXL0);
    
    json_object_object_get_ex(json_parsed1, "ADXL345_roll", &ADXL345_roll);
    const char* lvl_2_ADXL1 = json_object_get_string(ADXL345_roll);
    std::string lvl_2_ADXL2 = std::string(lvl_2_ADXL1);
	double ADXL_roll = (std::atof(lvl_2_ADXL2.c_str()));
    printf("Roll: %f\n",ADXL_roll);

    //----------------------------------------------------------
    std::fstream write;
    
    if(ADXL_roll > 30){
        printf("subsribe0 detected high roll value (exceeding 30)\n");
		//actuator application --> triggering an LED when a high roll value
		// is detected
        ledConditionOn(); // calling function above to set to default-on
        write.open (LED_PATH "/brightness", std::fstream::out);
        write << "255"; //setting brightness to 255 (or 1) in brightness folder
        write.close();
   }
   else {
        ledConditionOff();
        write.open (LED_PATH "/brightness", std::fstream::out);
        write << "0";
        write.close();
   }
    
    putchar('\n');
    MQTTClient_freeMessage(&message);
    MQTTClient_free(topicName);
    return 1;
}

void connlost(void *context, char *cause) {
    printf("\nConnection lost\n");
    printf("     cause: %s\n", cause);
}


int main(int argc, char* argv[]) {
    MQTTClient client;
    MQTTClient_connectOptions opts = MQTTClient_connectOptions_initializer;
    int rc;
    int ch;

    MQTTClient_create(&client, ADDRESS, CLIENTID, MQTTCLIENT_PERSISTENCE_NONE, NULL);
    opts.keepAliveInterval = 20;
    opts.cleansession = 0;  //persistent session if cleansession flag =0
    opts.username = AUTHMETHOD;
    opts.password = AUTHTOKEN;

    MQTTClient_setCallbacks(client, NULL, connlost, msgarrvd, delivered);
    if ((rc = MQTTClient_connect(client, &opts)) != MQTTCLIENT_SUCCESS) {
        printf("Failed to connect, return code %d\n", rc);
        exit(-1);
    }
    printf("Subscribing to topic %s\nfor client %s using QoS%d\n\n"
           "Press Q<Enter> to quit\n\n", TOPIC, CLIENTID, QOS);
    MQTTClient_subscribe(client, TOPIC, QOS);

    do {
        ch = getchar();
    } while(ch!='Q' && ch != 'q');
    MQTTClient_disconnect(client, 10000);
    MQTTClient_destroy(&client);
    return rc;
}
